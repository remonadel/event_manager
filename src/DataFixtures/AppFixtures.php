<?php

namespace App\DataFixtures;

use App\Domain\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
         $user = new User();
         $user->setName("Admin");
         $user->setEmail("admin@test.com");
         $user->setIsAdmin(true);
         $user->setPassword("123456");
         $manager->persist($user);

        $manager->flush();
    }
}
