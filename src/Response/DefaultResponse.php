<?php
/**
 * Created by PhpStorm.
 * User: remon
 * Date: 2/19/20
 * Time: 11:52 PM
 */

namespace App\Response;

use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultResponse
{
    public static function success($data, $message = '', $code = 200)
    {
        $result = [
            'success' => true,
            'data' => json_decode($data, true),
            'message' => $message,
            'code' => $code
        ];
        return JsonResponse::fromJsonString(json_encode($result), $code);
    }

    public static function error($message, $code = 400)
    {
        $code = ($code === 0) ? 400 : $code;
        $data = [
            'message' => $message
        ];
        return JsonResponse::create($data, $code);
    }
}
