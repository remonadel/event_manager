<?php

namespace App\Domain\Service;

use App\Domain\Repository\RepositoryInterface;

/**
 * Class AbstractService
 * @package App\Domain\Service
 * @author Remon Adel <r.adel@yellow.com.eg>
 */
abstract class AbstractService implements ServiceInterface
{
    /**
     * @var RepositoryInterface
     */
    protected $repository;

    /**
     * AbstractService constructor.
     * @param RepositoryInterface $repository
     */
    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return mixed
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return mixed
     */
    public function findOneBy(array $criteria, array $orderBy = null)
    {
        return $this->repository->findOneBy($criteria, $orderBy);
    }

    /**
     * @param array $filters
     * @param int   $offset
     * @param int   $limit
     *
     * @return mixed
     */
    public function findByDeepPaginated(array $filters, $offset = 0, $limit = 25)
    {
        return $this->repository->findByDeepPaginated($filters, $offset, $limit);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function create($data)
    {
        $entity = $this->repository->loadNew($data);
        return $this->repository->save($entity);
    }

    /**
     * @param $entity
     * @return mixed
     */
    public function save($entity)
    {
        return $this->repository->save($entity);
    }

    /**
     * @param $entityId
     * @param $post
     * @return mixed
     */
    public function update($entityId, $post)
    {
        $entity = $this->find($entityId);
        return $this->repository->update($entity, $post);
    }

    /**
     * @param $entityId
     * @return mixed
     */
    public function delete($entityId)
    {
        $entity = $this->find($entityId);
        return $this->repository->delete($entity);
    }

    /**
     * @param $parameter
     * @param $exceptionMessage
     * @throws \Exception
     */
    public function alreadyExists($parameter, $exceptionMessage)
    {
        $entity = $this->repository->findOneBy($parameter);
        if (!is_null($entity)) {
            throw new \Exception($exceptionMessage, 400);
        }
    }
}
