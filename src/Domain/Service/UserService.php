<?php

namespace App\Domain\Service;

use App\Domain\Entity\User;
use App\Domain\Repository\UserRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class UserService
 * @package App\Domain\Service
 */
class UserService extends AbstractService
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    
    /**
     * UserService constructor.
     * @param UserRepository $repository
     * @param TokenStorageInterface $storage
     */
    public function __construct(UserRepository $repository, TokenStorageInterface $storage)
    {
        $this->tokenStorage = $storage;
        parent::__construct($repository);
    }

    /**
     * @return User|null
     */
    public function getCurrentUser()
    {
        $token = $this->tokenStorage->getToken();
        if ($token instanceof TokenInterface) {

            /** @var User $user */
            $user = $token->getUser();
            return $user;
        } else {
            return null;
        }
    }
}
