<?php

namespace App\Domain\Service;

use App\Domain\Repository\EventRepository;

/**
 * Class EventService
 * @package App\Domain\Service
 */
class EventService extends AbstractService
{
    /**
     * EventService constructor.
     * @param EventRepository $repository
     */
    public function __construct(EventRepository $repository)
    {
        parent::__construct($repository);
    }
}
