<?php

namespace App\Domain\Service;

/**
 * Interface ServiceInterface
 * @package App\Domain\Service
 */
interface ServiceInterface
{
    /**
     * @param $id
     *
     * @return mixed
     */
    public function find($id);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return mixed
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return mixed
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @param array $filters
     * @param int   $offset
     * @param int   $limit
     *
     * @return mixed
     */
    public function findByDeepPaginated(array $filters, $offset = 0, $limit = 25);

    /**
     * @param $data
     *
     * @return mixed
     */
    public function create($data);

    /**
     * @param $entity
     * @return mixed
     */
    public function save($entity);

    /**
     * @param $entityId
     * @param $post
     *
     * @return mixed
     */
    public function update($entityId, $post);

    /**
     * @param $entityId
     *
     * @return mixed
     */
    public function delete($entityId);
}
