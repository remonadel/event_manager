<?php

namespace App\Domain\Entity;

use App\CoreDomain\Entity\Section;
use App\Domain\Entity\Traits\Blameable;
use App\Domain\Entity\Traits\Identifiable;
use App\Domain\Entity\Traits\Timestampable;
use App\Domain\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Domain\Repository\EventRepository")
 */
class Event extends Entity
{
    use Identifiable, Timestampable, Blameable;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank
     * @Groups({"list", "one"})
     */
    private $name;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"list", "one"})
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"list", "one"})
     */
    private $location;

    /**
     * @ORM\Column(type="date")
     * @Assert\NotBlank
     * @Groups({"list", "one"})
     */
    private $eventDate;

    /**
     * @ORM\Column(type="time")
     * @Assert\NotBlank
     * @Groups({"list", "one"})
     */
    private $fromTime;

    /**
     * @ORM\Column(type="time")
     * @Assert\NotBlank
     * @Groups({"list", "one"})
     */
    private $toTime;

    /**
     * @ORM\ManyToMany(targetEntity="User")
     * @ORM\JoinTable(name="event_users",
     *      joinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     * @Groups({"list", "one"})
     */
    private $users;

    /**
     * Event constructor.
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        parent::__construct();
    }

    /**
     * @return bool|null
     */
    public function getType(): ?bool
    {
        return $this->type;
    }

    /**
     * @param bool $type
     * @return $this
     */
    public function setType(bool $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     * @return $this
     */
    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return string
     */
    public function getEventDate(): string
    {
        $dateTime = $this->eventDate;
        return $dateTime->format('Y-m-d');
    }

    /**
     * @param \DateTime $eventDate
     * @return $this
     */
    public function setEventDate(\DateTime $eventDate): self
    {
        $this->eventDate = $eventDate;

        return $this;
    }

    /**
     * @return string
     */
    public function getFromTime(): string
    {
        $dateTime = $this->fromTime;
        return $dateTime->format('H:i:s');
    }

    /**
     * @param \DateTime $fromTime
     * @return $this
     */
    public function setFromTime(\DateTime $fromTime): self
    {
        $this->fromTime = $fromTime;

        return $this;
    }

    /**
     * @return string
     */
    public function getToTime(): string
    {
        $dateTime = $this->toTime;
        return $dateTime->format('H:i:s');
    }

    /**
     * @param \DateTime $toTime
     * @return $this
     */
    public function setToTime(\DateTime $toTime): self
    {
        $this->toTime = $toTime;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * This is fake method to tell entity manager which class is related to get all its elements and pass it to setMultiple
     *
     * @param User $user
     */
    public function addMultipleUsers(User $user)
    {
    }

    /**
     * @param User[] $users
     */
    public function setMultipleUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @param mixed $users
     */
    public function setUsers($users): void
    {
        $this->users = $users;
    }

    /**
     * @return bool
     */
    public function isMeetingType(): bool
    {
        if ($this->type == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function isCallType(): bool
    {
        if ($this->type == 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public function getUsersIds()
    {
        $userIds = [];
        foreach ($this->users as $user) {
            $userIds[] = $user->getId();
        }
        return $userIds;
    }
}
