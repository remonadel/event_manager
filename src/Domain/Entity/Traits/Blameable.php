<?php

namespace App\Domain\Entity\Traits;

use App\Domain\Entity\User;

/**
 * Trait Blameable
 * @package Domain\Entities\Traits
 */
trait Blameable
{
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id")
     * @Groups({"list", "one"})
     */
    protected $createdBy;

    /**
     * Set createdBy
     *
     * @param User|null $createdBy
     * @return mixed
     */
    public function setCreatedBy(User $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }
}
