<?php

namespace App\Domain\Entity\Traits;

/**
 * Trait Identifiable
 * @package App\Domain\Entity\Traits
 */
trait Identifiable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"list", "one"})
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
