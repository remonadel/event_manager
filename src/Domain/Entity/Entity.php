<?php

namespace App\Domain\Entity;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Entity
 * @package App\Domain\Entity
 */
abstract class Entity
{
    /**
     * Entity constructor.
     */
    public function __construct()
    {
        $this->setCreatedAtValue();
    }
}
