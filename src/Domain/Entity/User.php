<?php

namespace App\Domain\Entity;

use App\Domain\Entity\Traits\Blameable;
use App\Domain\Entity\Traits\Identifiable;
use App\Domain\Entity\Traits\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Lexik\Bundle\JWTAuthenticationBundle\Security\User\JWTUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Domain\Repository\UserRepository")
 */
class User extends Entity implements JWTUserInterface
{
    use Identifiable, Timestampable, Blameable;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank
     * @Groups({"list", "one"})
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank
     * @Assert\Email
     * @Groups({"list", "one"})
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Assert\NotBlank
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", options={"default": 0}, nullable=true)
     */
    private $isAdmin;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = password_hash($password, PASSWORD_DEFAULT);
    }

    /**
     * @return mixed
     */
    public function isAdmin()
    {
        return $this->isAdmin ? true: false;
    }

    /**
     * @param mixed $isAdmin
     */
    public function setIsAdmin($isAdmin): void
    {
        $this->isAdmin = $isAdmin;
    }

    /**
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string|null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->email;
    }

    /**
     *
     */
    public function eraseCredentials()
    {
    }

    /**
     * @param string $email
     * @param array $payload
     * @return User|JWTUserInterface
     */
    public static function createFromPayload($email, array $payload)
    {
        return new self(
        );
    }

    /**
     * @return array|string[]
     * @Groups({"list", "one"})
     */
    public function getRoles()
    {
        if ($this->isAdmin()) {
            return [
                'ROLE_ADMIN'
            ];
        } else {
            return [
                'ROLE_USER'
            ];
        }
    }
}
