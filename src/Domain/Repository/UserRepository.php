<?php

namespace App\Domain\Repository;

use App\Domain\Entity\User;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class UserRepository
 * @package App\Domain\Repository
 */
class UserRepository extends AbstractRepository
{
    /**
     * UserRepository constructor.
     * @param ManagerRegistry $registry
     * @param ValidatorInterface $validator
     */
    public function __construct(ManagerRegistry $registry, ValidatorInterface $validator)
    {
        parent::__construct($registry, User::class, $validator);
    }
}
