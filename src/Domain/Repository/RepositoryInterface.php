<?php

namespace App\Domain\Repository;

/**
 * Interface RepositoryInterface
 * @package App\Domain\Repository
 */
interface RepositoryInterface
{
    /**
     * @param      $id
     * @param null $lockMode
     * @param null $lockVersion
     *
     * @return mixed
     */
    public function find($id, $lockMode = null, $lockVersion = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     * @param null       $limit
     * @param null       $offset
     *
     * @return mixed
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    /**
     * @param array      $criteria
     * @param array|null $orderBy
     *
     * @return mixed
     */
    public function findOneBy(array $criteria, array $orderBy = null);

    /**
     * @param array $filters
     * @param int   $offset
     * @param int   $limit
     *
     * @return mixed
     */
    public function findByDeepPaginated(array $filters, $offset = 0, $limit = 20);

    /**
     * @param      $entity
     * @param null $arrAttributes
     *
     * @return mixed
     */
    public function load($entity, $arrAttributes = null);

    /**
     * @param null $arrAttributes
     * @return mixed
     */
    public function loadNew($arrAttributes = null);

    /**
     * @param $entity
     * @param $arrAttributes
     *
     * @return mixed
     */
    public function update($entity, $arrAttributes);

    /**
     * @param $entity
     * @return mixed
     */
    public function save($entity);

    /**
     * @param $entities
     * @return mixed
     */
    public function saveMultiple($entities);

    /**
     * @param $entity
     * @return mixed
     */
    public function delete($entity);

    /**
     * @param $entities
     * @return mixed
     */
    public function deleteMultiple($entities);
}
