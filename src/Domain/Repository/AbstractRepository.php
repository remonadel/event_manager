<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Entity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Zend\Code\Generator\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractRepository
 * @package App\Domain\Repository
 * @author Remon Adel <r.adel@yellow.com.eg>
 */
abstract class AbstractRepository extends ServiceEntityRepository implements RepositoryInterface
{
    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * AbstractRepository constructor.
     * @param ManagerRegistry $registry
     * @param $entityClass
     * @param ValidatorInterface $validator
     */
    public function __construct(ManagerRegistry $registry, $entityClass, ValidatorInterface $validator)
    {
        $this->validator = $validator;
        parent::__construct($registry, $entityClass);
    }

    /**
     * @return array|string[]
     */
    protected function getAllEntityFieldsNames()
    {
        return $this->_em->getClassMetadata($this->_entityName)->getFieldNames();
    }

    /**
     * @return array|string[]
     */
    protected function getAllEntityAssociationNames()
    {
        return $this->_em->getClassMetadata($this->_entityName)->getAssociationNames();
    }

    /**
     * @param $entityNamespace
     * @param $entityId
     * @return object|null
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    private function findExternal($entityNamespace, $entityId)
    {
        $entityLoaded = $this->_em->find($entityNamespace, $entityId);
        return $entityLoaded;
    }

    /**
     * @param $propertyName
     * @return string|null
     */
    private function getTypeOfEntityPropertyByName($propertyName)
    {
        return $this->_em->getClassMetadata($this->_entityName)->getTypeOfField($propertyName);
    }

    /**
     * @param $entity
     * @param $arrAttributes
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \ReflectionException
     */
    private function setAttributes($entity, $arrAttributes)
    {
        $dateTimeTypes = ['date', 'time', 'datetime'];
        foreach ($arrAttributes as $attr => $val) {
            $attrType = $this->getTypeOfEntityPropertyByName($attr);

            if (method_exists($entity, 'set'.$attr)) {
                $param = new \ReflectionParameter(array(get_class($entity), 'set'.$attr), 0);

                if (!is_null($param->getClass()) && $param->getClass() instanceof Entity) {
                    $entityClass = $param->getClass()->name;

                    $value = $this->findExternal($entityClass, $val);
                    $entity->{'set'.$attr}($value);
                } else {
                    if (in_array($attrType, $dateTimeTypes)) {
                        $val = new \DateTime($val);
                    }
                    $entity->{'set'.$attr}($val);
                }
            }
        }
    }

    /**
     * @param $entity
     * @param $arrAttributes
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \ReflectionException
     */
    private function addRelatedEntity($entity, $arrAttributes)
    {
        foreach ($arrAttributes as $parentAttribute => $values) {
            if (is_array($values)) {
                foreach ($values as $val) {
                    if (method_exists($entity, 'add'.$parentAttribute)) {
                        $param = new \ReflectionParameter(array($this->_entityName, 'add'.$parentAttribute), 0);

                        if (!is_null($param->getClass())) {
                            $entityClass = $param->getClass()->name;
                            $newRelatedEntity  = new $entityClass;

                            $this->setAttributes($newRelatedEntity, $val);

                            $parentClassId  = get_class($entity);
                            $parentClassId = substr($parentClassId, strrpos($parentClassId, '\\') + 1);

                            $newRelatedEntity->{'set'.$parentClassId}($entity);

                            $entity->{'add'.$parentAttribute}($newRelatedEntity);
                        }
                    } elseif (method_exists($entity, 'addMultiple'.$parentAttribute)) {
                        $param = new \ReflectionParameter(array($this->_entityName, 'addMultiple'.$parentAttribute), 0);
                        if (! is_null($param->getClass())) {
                            $entityClass = $param->getClass()->name;
                            $allRefrencies = [];
                            foreach ($values as $value) {
                                $allRefrencies[] = $this->findExternal($entityClass, $value);
                            }
                            $entity->{'setMultiple'.$parentAttribute}($allRefrencies);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $entity
     * @param null $arrAttributes
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \ReflectionException
     */
    public function load($entity, $arrAttributes = null)
    {
        if ($arrAttributes) {
            $this->setAttributes($entity, $arrAttributes);
            $this->loadRelatedEntity($entity, $arrAttributes);
        }

        return $entity;
    }

    /**
     * @param null $arrAttributes
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \ReflectionException
     */
    public function loadNew($arrAttributes = null)
    {
        $entity =  new $this->_entityName();
        if ($arrAttributes) {
            $this->setAttributes($entity, $arrAttributes);
            $this->addRelatedEntity($entity, $arrAttributes);
        }

        $errors = $this->validator->validate($entity);

        if (count($errors) > 0) {
            $errorsString = $errors->__toString();

            throw new InvalidArgumentException($errorsString);
        }
        return $entity;
    }

    /**
     * @param $entity
     * @param $arrAttributes
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \ReflectionException
     */
    private function loadRelatedEntity($entity, $arrAttributes)
    {
        foreach ($arrAttributes as $attr => $val) {
            if (is_array($val)) {
                foreach ($val as $id) {
                    if (method_exists($entity, 'add'.$attr)) {
                        $param = new \ReflectionParameter(array($this->_entityName, 'add'.$attr), 0);

                        if (!is_null($param->getClass())) {
                            $entityClass = $param->getClass()->name;

                            $value = $this->findExternal($entityClass, $id);
                            $entity->{'add'.$attr}($value);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $entities
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveMultiple($entities)
    {
        foreach ($entities as $entity) {
            $this->_em->persist($entity);
        }
        $this->_em->flush();
        return $entities;
    }

    /**
     * @param $entity
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save($entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush();
        return $entity;
    }

    /**
     * @param $entity
     * @param $arrAttributes
     * @return mixed
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \ReflectionException
     */
    public function update($entity, $arrAttributes)
    {
        $acceptedAttributes = [];
        foreach ($arrAttributes as $key => $value) {
            if (in_array($key, $this->getAllEntityFieldsNames()) ||
                in_array($key, $this->getAllEntityAssociationNames())) {
                $acceptedAttributes[$key] = $value;
            }
        }
        $this->setAttributes($entity, $acceptedAttributes);
        $this->addRelatedEntity($entity, $acceptedAttributes);
        return $this->save($entity);
    }

    /**
     * @param QueryBuilder $qb
     * @param $filters
     * @return QueryBuilder
     */
    protected function filterByQuery(QueryBuilder $qb, $filters)
    {
        if (isset($filters['q']) && ! empty($filters['q']) && isset($filters['fieldsNames']) && ! empty($filters['fieldsNames'])) {
            $searchedQuery = $filters['q'];
            $fieldsNames = array_filter(explode(',', $filters['fieldsNames']));
            if (! empty($fieldsNames)) {
                $orX = $qb->expr()->orX();
                foreach ($fieldsNames as $key => $fieldName) {
                    if (in_array($fieldName, $this->getAllEntityFieldsNames())) {
                        $orX->add("{$qb->getAllAliases()[0]}.{$fieldName} LIKE :{$fieldName}{$key}");
                        $qb->setParameter($fieldName . $key, '%' . $searchedQuery . '%');
                    }
                }
                if ($orX->getParts()) {
                    $qb->andWhere($qb->expr()->orX($orX));
                }
            }
        }
        return $qb;
    }

    /**
     * @param array $filters
     * @param int $offset
     * @param int $limit
     * @return array|mixed
     */
    public function findByDeepPaginated(array $filters, $offset = 0, $limit = 20)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('e')->from($this->_entityName, 'e');

        $this->filterByQuery($qb, $filters);

        if (! empty($filters)) {
            foreach ($filters as $key => $value) {
                if ((in_array($key, $this->getAllEntityFieldsNames()) || in_array($key, $this->getAllEntityAssociationNames())) && ! is_array($filters[$key])) {
                    $qb->andWhere("e.$key = :$key");
                    $qb->setParameter($key, $value);
                } elseif (in_array($key, $this->getAllEntityAssociationNames())) {
                    $qb->andWhere($qb->expr()->isMemberOf(":{$key}", "e.{$key}"))
                        ->setParameter($key, $value);
                }
            }
        }

        if (isset($filters['notEqual']) && ! empty($filters['notEqual'])) {
            foreach ($filters['notEqual'] as $key => $value) {
                $qb->andWhere($qb->expr()->notIn("e.$key", $value));
            }
        }

        if (isset($filters['notEqualAssociation']) && ! empty($filters['notEqualAssociation'])) {
            foreach ($filters['notEqualAssociation'] as $key => $value) {
                $qb->andWhere("e.$key != :$key")->setParameter(":$key", $value);
            }
        }

        if (isset($filters['hasDateRange']) && isset($filters['dateRange']['field']) && isset($filters['dateRange']['from']) && isset($filters['dateRange']['to'])) {
            $qb->andWhere("e.{$filters['dateRange']['field']} BETWEEN :from AND :to")
                ->setParameter('from', $filters['dateRange']['from'])
                ->setParameter('to', $filters['dateRange']['to']);
        }

        if (isset($filters['orderBy']) && !empty($filters['orderBy']) && isset($filters['order']) && !empty($filters['order'])) {
            $orderByAccepted = ['asc', 'ASC', 'DESC', 'desc'];
            if (in_array($filters['order'], $orderByAccepted) && in_array($filters['orderBy'], $this->getAllEntityFieldsNames())) {
                $qb->orderBy("e.{$filters['orderBy']}", $filters['order']);
            } else {
                $qb->orderBy("e.id", 'ASC');
            }
        } else {
            $qb->orderBy("e.id", 'ASC');
        }


        $pagination = (isset($filters['pagination'])) ? true : false;
        $page = (isset($filters['page'])) ? (int) $filters['page'] : 1;
        return $this->getData($qb, $offset, $limit, $page, $pagination);
    }

    /**
     * @param QueryBuilder $qb
     * @param $offset
     * @param $limit
     * @param $page
     * @param bool $pagination
     * @return array
     */
    private function getData(QueryBuilder $qb, $offset, $limit, $page, $pagination = false)
    {
        if ($limit != 'all') {
            $limit = ($limit <= 300) ? $limit : 20;
        }

        if (! $pagination) {
            if ($limit != 'all') {
                $qb->setFirstResult($offset)->setMaxResults($limit);
            }
            return $qb->getQuery()->getResult();
        }

        //for count
        $countQb = clone $qb;
        $countQb->select('count(e.id)');


        if ($limit != 'all') {
            $qb->setFirstResult($offset)->setMaxResults($limit);
        }

        $items = $qb->getQuery()->getResult();
        return [
            'items' => $items,
            'paging' => [
                'total' => (int) $countQb->getQuery()->getSingleScalarResult(),
                'currentPage' => $page,
                'perPage' => count($items)
            ]
        ];
    }

    /**
     * @param $entity
     * @return mixed|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function delete($entity)
    {
        $this->_em->remove($entity);
        $this->_em->flush();
    }

    /**
     * @param $entities
     * @return mixed|void
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteMultiple($entities)
    {
        foreach ($entities as $entity) {
            $this->_em->remove($entity);
        }
        $this->_em->flush();
    }
}
