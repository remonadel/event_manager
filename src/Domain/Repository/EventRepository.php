<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Event;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class EventRepository
 * @package App\Domain\Repository
 */
class EventRepository extends AbstractRepository
{
    /**
     * EventRepository constructor.
     * @param ManagerRegistry $registry
     * @param ValidatorInterface $validator
     */
    public function __construct(ManagerRegistry $registry, ValidatorInterface $validator)
    {
        parent::__construct($registry, Event::class, $validator);
    }
}
