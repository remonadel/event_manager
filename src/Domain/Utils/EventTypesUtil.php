<?php

namespace App\Domain\Utils;

/**
 * Class EventTypesUtil
 * @package App\Domain\Utils
 */
class EventTypesUtil
{
    /**
     * Event type meeting max users count
     */
    const EVENT_TYPE_MEETING = 4;

    /**
     * Event type call max users count
     */
    const EVENT_TYPE_CALL = 2;
}
