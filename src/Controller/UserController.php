<?php

namespace App\Controller;

use App\Domain\Service\UserService;
use App\Response\DefaultResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserController
 * @package App\Controller
 */
class UserController extends AbstractActionController
{
    /**
     * UserController constructor.
     * @param UserService $service
     */
    public function __construct(UserService $service)
    {
        parent::__construct($service, $service);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $post = json_decode($request->getContent(), true);
        $post = $this->removeEmpty($post);
        $post['createdBy'] = $this->service->getCurrentUser();
        if (isset($post['email']) && !empty($post['email'])) {
            try {
                $this->service->alreadyExists(['email' => $post['email']], 'This email is already exists!');
            } catch (\Exception $e) {
                return DefaultResponse::error($e->getMessage(), $e->getCode());
            }
        }

        try {
            $data = $this->service->create($post);
            $entity = $this->serialize($data, ['one']);
            return DefaultResponse::success($entity, 'Success !');
        } catch (\Exception $e) {
            return DefaultResponse::error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $post = json_decode($request->getContent(), true);
        $post = $this->removeEmpty($post);
        if (isset($post['email']) && !empty($post['email'])) {
            try {
                $this->service->alreadyExists(['email' => $post['email']], 'This email is already exists!');
            } catch (\Exception $e) {
                return DefaultResponse::error($e->getMessage(), $e->getCode());
            }
        }

        try {
            $data = $this->service->create($post);
            $entity = $this->serialize($data, ['one']);
            return DefaultResponse::success($entity, 'Success !');
        } catch (\Exception $e) {
            return DefaultResponse::error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @return JsonResponse
     */
    public function me()
    {
        $user = $this->service->getCurrentUser();
        $user = $this->serialize($user, ['one']);
        return DefaultResponse::success($user);
    }
}
