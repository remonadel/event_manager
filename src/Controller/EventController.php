<?php

namespace App\Controller;

use App\Domain\Service\EventService;
use App\Domain\Service\UserService;
use App\Domain\Utils\EventTypesUtil;
use App\Response\DefaultResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class EventController
 * @package App\Controller
 */
class EventController extends AbstractActionController
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * EventController constructor.
     * @param EventService $service
     * @param UserService $userService
     */
    public function __construct(EventService $service, UserService $userService)
    {
        parent::__construct($service, $userService);
        $this->userService = $userService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function all(Request $request)
    {
        $filters = $request->query->all();
        $limit = 25;
        $offset = 0;

        if (isset($filters['limit']) && (is_numeric($filters['limit']) || $filters['limit'] == 'all')) {
            $limit = $filters['limit'];
        }

        if (isset($filters['page']) && is_numeric($filters['page']) && is_numeric($limit)) {
            $offset = $filters['page'] <= 0 ? 0 : ($filters['page'] - 1) * $limit;
        }

        $currentUser = $this->userService->getCurrentUser();
        if (!$currentUser->isAdmin()) {
            $filters['createdBy'] = $currentUser;
        }
        $data = $this->service->findByDeepPaginated($filters, $offset, $limit);
        $data = $this->serialize($data, ['list']);
        return DefaultResponse::success($data);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    public function myEvents(Request $request)
    {
        $filters = $request->query->all();
        $limit = 25;
        $offset = 0;

        if (isset($filters['limit']) && (is_numeric($filters['limit']) || $filters['limit'] == 'all')) {
            $limit = $filters['limit'];
        }

        if (isset($filters['page']) && is_numeric($filters['page']) && is_numeric($limit)) {
            $offset = $filters['page'] <= 0 ? 0 : ($filters['page'] - 1) * $limit;
        }
        $filters['users'] = [$this->userService->getCurrentUser()];
        $data = $this->service->findByDeepPaginated($filters, $offset, $limit);
        $data = $this->serialize($data, ['list']);
        return DefaultResponse::success($data);
    }

    /**
     * @param Request $request
     * @param $eventId
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function addUserToEvent(Request $request, $eventId)
    {
        $post = json_decode($request->getContent(), true);
        $post = $this->removeEmpty($post);
        $currentUser = $this->userService->getCurrentUser();

        $event = $this->service->find($eventId);
        if (!$event) {
            return DefaultResponse::error("Undefined Event!");
        }

        if (!$currentUser->isAdmin() && $currentUser->getId() != $event->getCreatedBy()->getId()) {
            return DefaultResponse::error("You don't have permission to do this action!");
        }
        $eventUserIds = $event->getUsersIds();

        if ($event->isMeetingType()) {
            $maxNoOfUsers = EventTypesUtil::EVENT_TYPE_MEETING;
        } else {
            $maxNoOfUsers = EventTypesUtil::EVENT_TYPE_CALL;
        }
        //-- check if the event reached the maximum no of users
        if (count($eventUserIds) == $maxNoOfUsers) {
            return DefaultResponse::error("You have reached to the maximum no of users for this event!");
        }
        //-- search for users by the given data
        try {
            $user = $this->userService->findBy(['name' => $post['name'], 'email' => $post['email']]);
            if (count($user) > 0) {
                $user = current($user);
                if (in_array($user->getId(), $eventUserIds)) {
                    return DefaultResponse::error("This user is already added to the event!");
                }
                $eventUsers = $event->getUsers();
                $eventUsers[] = $user;
                $event->setMultipleUsers($eventUsers);
            } else {
                return DefaultResponse::error("You wrote an invalid Name or Email!", 404);
            }
        } catch (\Exception $e) {
            return DefaultResponse::error($e->getMessage(), $e->getCode());
        }

        $event = $this->service->save($event);
        $event = $this->serialize($event, ['one']);
        return DefaultResponse::success($event);
    }
}
