<?php
namespace App\Controller;

use App\Domain\Service\ServiceInterface;
use App\Domain\Service\UserService;
use App\Response\DefaultResponse;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class AbstractActionController
 * @package App\Controller
 */
abstract class AbstractActionController extends AbstractController
{
    /**
     * @var ServiceInterface
     */
    protected $service;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * AbstractActionController constructor.
     * @param ServiceInterface $service
     * @param UserService $userService
     */
    public function __construct(ServiceInterface $service, UserService $userService)
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];

        $this->serializer = new Serializer($normalizers, $encoders);
        $this->service = $service;
        $this->userService = $userService;
    }

    /**
     * @param $haystack
     * @return mixed
     */
    protected function removeEmpty($haystack)
    {
        if (! empty($haystack)) {
            foreach ($haystack as $key => $value) {
                if (is_array($value)) {
                    $haystack[$key] = $this->removeEmpty($haystack[$key]);
                }

                if (!is_numeric($haystack[$key]) && empty($haystack[$key])) {
                    unset($haystack[$key]);
                }
            }
        }

        return $haystack;
    }

    /**
     * @param $data
     * @param array $groups
     * @return string
     * @throws \Doctrine\Common\Annotations\AnnotationException
     */
    protected function serialize($data, $groups = [])
    {
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);
        $serializer = new Serializer([new ObjectNormalizer($classMetadataFactory, $metadataAwareNameConverter)], ['json' => new JsonEncoder()]);

        $groups = (! empty($groups)) ? ['groups' => $groups]: [];
        return $serializer->serialize($data, 'json', $groups);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function all(Request $request)
    {
        $filters = $request->query->all();
        $limit = 25;
        $offset = 0;

        if (isset($filters['limit']) && (is_numeric($filters['limit']) || $filters['limit'] == 'all')) {
            $limit = $filters['limit'];
        }

        if (isset($filters['page']) && is_numeric($filters['page']) && is_numeric($limit)) {
            $offset = $filters['page'] <= 0 ? 0 : ($filters['page'] - 1) * $limit;
        }

        $data = $this->service->findByDeepPaginated($filters, $offset, $limit);
        $data = $this->serialize($data, ['list']);
        return DefaultResponse::success($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function one(Request $request, $id)
    {
        if ((int) $id > 0) {
            $entity = $this->service->find($id);

            if ($entity) {
                $entity = $this->serialize($entity, ['one']);
                return DefaultResponse::success($entity, 'Success !');
            }
        }

        return DefaultResponse::error("Not found Entity!");
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $post = json_decode($request->getContent(), true);
        $post = $this->removeEmpty($post);
        $post['createdBy'] = $this->userService->getCurrentUser();
        try {
            $data = $this->service->create($post);
            $entity = $this->serialize($data, ['one']);
            return DefaultResponse::success($entity, 'Success !');
        } catch (\Exception $e) {
            return DefaultResponse::error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param Request $request
     * @param $entityId
     * @return JsonResponse
     */
    public function update(Request $request, $entityId)
    {
        $post = json_decode($request->getContent(), true);
        $post['updatedBy'] = $this->userService->getCurrentUser();
        try {
            $data = $this->service->update($entityId, $post);
            $entity = $this->serialize($data, ['one']);
            return DefaultResponse::success($entity, 'Success !');
        } catch (\Exception $e) {
            return DefaultResponse::error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $entityId
     * @return JsonResponse
     */
    public function delete($entityId)
    {
        try {
            $this->service->delete($entityId);

            return DefaultResponse::success('', 'Deleted Successfully!');
        } catch (\Exception $e) {
            return DefaultResponse::error($e->getMessage(), $e->getCode());
        }
    }
}
