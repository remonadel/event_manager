# event-manager-api
Before going through the setup you need to update the .env file with your database credentials.
## Project setup
```
composer install
php bin/console doctrine:schema:update 
php -S localhost:8000 -t public/
```

### run project fixtures
```
php bin/console doctrine:fixtures:load
```

### Login to application
```
email:admin@test.com
password: 123456
```

### To register a new user
just open your frontend application URL/register

